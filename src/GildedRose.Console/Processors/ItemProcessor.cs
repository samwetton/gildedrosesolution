﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GildedRose.Console.Processors
{
    public class ItemProcessor
    {
        protected Item item;

        public ItemProcessor(Item item)
        {
            this.item = item;
        }

        public void Process()
        {
            ChangeQuantity();
            ReduceSellIn();
        }

        protected virtual void ChangeQuantity()
        {
            if (IsExpired())
            {
                if (item.Quality > 0)
                {
                    item.Quality -= 2;
                }
            }
            else
            {
                if (item.Quality > 0)
                {
                    item.Quality -= 1;
                }
            }
        }

        protected virtual void ReduceSellIn()
        {
            item.SellIn -= 1;
        }

        protected virtual Boolean IsUnderMaxQuality()
        {
            return item.Quality < 50;
        }

        protected virtual Boolean IsExpired()
        {
            return item.SellIn <= 0;
        }
    }
}
