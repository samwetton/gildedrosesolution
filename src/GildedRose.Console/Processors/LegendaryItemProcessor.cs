﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GildedRose.Console.Processors
{
    public class LegendaryItemProcessor : ItemProcessor
    {
        public LegendaryItemProcessor(Item item) : base(item)
        {}

        protected override void ChangeQuantity()
        {

        }

        protected override void ReduceSellIn()
        {

        }


    }
}
