﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GildedRose.Console.Processors
{
    public class BackstagePassProcessor : ItemProcessor {
        public BackstagePassProcessor(Item item) : base(item)
        {}

        protected override void ChangeQuantity()
        {
            if (IsExpired())
            {
                item.Quality = 0;
            }
            else
            {
                if (IsUnderMaxQuality()) 
                {
                    item.Quality += 1;
                }

                if (item.SellIn < 6) 
                {
                    if (IsUnderMaxQuality()) 
                    {
                        item.Quality += 1;
                    }
                }

                if (item.SellIn < 11) 
                {
                    if (IsUnderMaxQuality()) 
                    {
                        item.Quality += 1;
                    }
                }
            }
        }

    }
}
